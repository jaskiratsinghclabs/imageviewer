//
//  ViewController.swift
//  image viewer
//
//  Created by Click Labs 65 on 1/13/15.
//  Copyright (c) 2015 clicklabs. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    

    @IBOutlet weak var imageViewer: UIImageView!
    
    
    @IBOutlet weak var textField: UITextField!
    
    
    //whenever submit button is pressed switch case is performed to which background changes
    
    @IBAction func submit(sender: AnyObject) {
        
        var num = textField.text.toInt()
        
        switch(num!){
            
        case 1 :        
            var image = UIImage(named: "landscape1")
            imageViewer.image = image
            
            
       case 2 :
            var image = UIImage(named: "landscape2")
            imageViewer.image = image
       
        case 3 :
            var image = UIImage(named: "landscape3")
            imageViewer.image = image
       
        case 4 :
            var image = UIImage(named: "landscape4")
            imageViewer.image = image
       
        case 5 :
            var image = UIImage(named: "landscape5")
            imageViewer.image = image
       
        case 6 :
            var image = UIImage(named: "landscape6")
            imageViewer.image = image
        
        case 7 :
            var image = UIImage(named: "landscape7")
            imageViewer.image = image
       
        case 8 :
            var image = UIImage(named: "landscape8")
            imageViewer.image = image
        
        case 9 :
            var image = UIImage(named: "landscape9")
            imageViewer.image = image
        
        case 10 :
            var image = UIImage(named: "landscape10")
            imageViewer.image = image
            
            
            
        default :
            
            var image = UIImage(named: "landscape11")
            imageViewer.image = image


        }
        
            }
    
    
    
       
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

